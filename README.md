# Yaz0 CLI
![alt Ice-Spear](logo.png)

Yaz0-CLI is a command line tool to en/decode Yaz0 files.

It's using NodeJS and the yaz0-lib from the same project-group.

## Installation
To install it globally run:
```
npm install -g gitlab:ice-spear-tools/yaz0-cli
```
## Usage
A help page can be shown with:
```
yaz0 --help
```
When installed globally, you can use it with
```
yaz0 [encode|decode] [fileIn] <fileOut>
```
The "fileOut" parameter at the end is optional, if set, it's writing the result to the set file.

If empty, the result is printed directly to the stdout and can be used to pipe it.

