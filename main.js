#!/usr/bin/env node

/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const program = require("commander");
const fs      = require("fs");
const yaz0    = require("yaz0-lib");

const EXIT_SUCCESS = 0;
const EXIT_ERROR   = 1;

function handleError(err)
{
    process.stderr.write(err + "");
    process.exit(EXIT_ERROR);
}

program.version('0.1.0')
    .description("CLI for creating and parsing Yaz0 files.\n" + 
               "  © Max Bebök - 2018 - as part of 'Ice-Spear Tools'");

program.command('decode <yaz0File> [fileOut]')
    .description('decodes a Yaz0 file back into the original file')
    .action(function (yaz0File, fileOut) 
    {
        fs.readFile(yaz0File, (err, yaz0Buffer) => 
        {
            if(err)return handleError(err);

            const dataBuffer = yaz0.decode(yaz0Buffer);
            if(fileOut == null)
            {
                process.stdout.write(dataBuffer);
            }else{
                fs.writeFile(fileOut, dataBuffer, (err) => {
                    if(err)return handleError(err);
                    process.exit(EXIT_SUCCESS);
                });
            }
        });
    });

program.command('encode <fileIn> [yazOut]')
    .description("encodes a file into an Yaz0 file")
    .action(function (fileIn, yazOut) 
    {
        fs.readFile(fileIn, (err, fileIn) => 
        {
            if(err)return handleError(err);

            const yaz0Buffer = yaz0.encode(fileIn);
            if(yazOut == null)
            {
                process.stdout.write(yaz0Buffer);
            }else{
                fs.writeFile(yazOut, yaz0Buffer, (err) => {
                    if(err)return handleError(err);
                    process.exit(EXIT_SUCCESS);
                });
            }
        });
    });

program.parse(process.argv);